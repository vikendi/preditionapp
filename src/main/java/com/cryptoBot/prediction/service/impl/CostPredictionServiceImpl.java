package com.cryptoBot.prediction.service.impl;

import com.cryptoBot.prediction.dto.PredictionParams;
import com.cryptoBot.prediction.helpers.PredictionCore;
import com.cryptoBot.prediction.model.CryptoParams;
import com.cryptoBot.prediction.service.CostPredictionService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class CostPredictionServiceImpl implements CostPredictionService {

    @Override
    public double defineCost(PredictionParams params) {
        PredictionCore predictTest = new PredictionCore();
        double result;
        result = predictTest.predictValue(params, null);
        return result;
    }

    @Override
    public void AddRow(double EnCons, double Supply, double MinDif, double ChangeDin, double TransactionPerDay) {

    }
}
