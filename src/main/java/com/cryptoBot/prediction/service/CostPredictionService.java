package com.cryptoBot.prediction.service;

import com.cryptoBot.prediction.dto.PredictionParams;

public interface CostPredictionService {

    public double defineCost(PredictionParams params);

    public void AddRow(double EnCons, double Supply, double MinDif, double ChangeDin, double TransactionPerDay);
}
