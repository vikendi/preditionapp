package com.cryptoBot.prediction.helpers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StatisticsReader {
    private BufferedReader reader;
    private List<double[]> XValues;
    private List<Double> Yvalues;


    private StatisticsReader(BufferedReader source) {
        this.reader = source;
        this.XValues = new ArrayList<double[]>();
        this.Yvalues = new ArrayList<Double>();
    }

    private void init() throws IOException {
        try {
            String line;
            this.reader.readLine(); // this will read the first line (need to skip header of file)
            while ((line = reader.readLine()) != null) {
                double[] doubleValues = Arrays.stream(line.split("\t"))
                        .mapToDouble(Double::parseDouble)
                        .toArray();
                this.Yvalues.add(doubleValues[doubleValues.length - 1]);
                this.XValues.add(Arrays.copyOf(doubleValues, doubleValues.length - 1));
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            reader.close();
        }
    }

    public static StatisticsReader create(String dataSource) throws IOException {
        BufferedReader source = new BufferedReader(new FileReader(dataSource));
        StatisticsReader Sreader = new StatisticsReader(source);
        Sreader.init();
        return Sreader;
    }

    public double[] getYValues() {
        return Yvalues.stream().mapToDouble(i -> i).toArray();
    }

    public double[][] getXValues() {
        double[][] xVals = new double[XValues.size()][];
        for (int i = 0; i < XValues.size(); i++) {
            double[] row = XValues.get(i);
            xVals[i] = row;
        }
        return xVals;
    }
}
