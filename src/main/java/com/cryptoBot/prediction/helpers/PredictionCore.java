package com.cryptoBot.prediction.helpers;

import com.cryptoBot.prediction.dto.PredictionParams;
import com.cryptoBot.prediction.model.CryptoParams;
import org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression;

import java.io.IOException;

public class PredictionCore {
	private static final String DEFAULT_SOURCE = "C:\\Users\\Nikita\\Desktop\\DIPLOM\\DataToAnalys.txt";
	public double predictValue(PredictionParams params, String src) {
		String source = src == null	? DEFAULT_SOURCE : src;
		double result;
		double [] regrParams;
		try {
			StatisticsReader statData = StatisticsReader.create(source);
			regrParams = calculateOlsRegression(statData.getXValues(), statData.getYValues());

			result = regrParams[0] +  regrParams[1]*params.getEnCons() + regrParams[2]*params.getSupply() + regrParams[3]*params.getMinDif()
					+ regrParams[4]*params.getChangeDin() + regrParams[5] * params.getTransactionPerDay();

//EnCons:0  - Energy Consumption
//Supply:1 - Current number of bitcoins
//MinDif:2 - Difficulty of hash calculating
//ChangeDin:3 - Change dynamics
//TransactionPerDay:4 - transactions per day
			return result;
		}
		catch (IOException e) {
			System.err.println("Invalid statistic sourse");
			e.printStackTrace();
			return 0;
		}
		//double test = beta[0] + beta[1]*43.8 + beta[2]*17.5982 + beta[3]* 5968891+ beta[4]* 25.23 + beta[5] * 291944;

	}
	private double[] calculateOlsRegression(double[][] x, double[] y) {
		OLSMultipleLinearRegression regression = new OLSMultipleLinearRegression();
		regression.newSampleData(y, x);
		return regression.estimateRegressionParameters();
	}
}
