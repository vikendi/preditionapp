package com.cryptoBot.prediction.model;

/**
 * Enumeration that represents status of domain objects - ACTIVE, DELETED, etc.
 *
 * @author Nikita Tikhonov
 * @version 1.0
 */

public enum Status {
    ACTIVE, NOT_ACTIVE, DELETED
}