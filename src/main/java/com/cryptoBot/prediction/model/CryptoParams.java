package com.cryptoBot.prediction.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class CryptoParams {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private double enCons;


    @Column
    private double supply;

    @Column
    private double minDif;

    @Column
    private double changeDin;

    @Column
    private double transactionPerDay;

    public Long getId() {
        return id;
    }

    public void setId(Long id) { this.id = id; }

    public double getEnCons() { return enCons; }

    public void setEnCons(double enCons) { this.enCons = enCons; }

    public double getSupply() { return supply; }

    public void setSupply(double supply) { this.supply = supply; }

    public double getMinDif() { return minDif; }

    public void setMinDif(double minDif) { this.minDif = minDif; }

    public double getChangeDin() { return changeDin; }

    public void setChangeDin(double changeDin) { this.changeDin = changeDin; }

    public double getTransactionPerDay() { return transactionPerDay; }

    public void setTransactionPerDay(double transactionPerDay) { this.transactionPerDay = transactionPerDay; }
}
