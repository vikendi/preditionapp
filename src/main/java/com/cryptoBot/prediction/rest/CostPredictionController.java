package com.cryptoBot.prediction.rest;

import com.cryptoBot.prediction.dto.PredictionParams;
import com.cryptoBot.prediction.model.CryptoParams;
import com.cryptoBot.prediction.service.CostPredictionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/")

public class CostPredictionController {

    private final CostPredictionService predictionService;

    @Autowired
    public CostPredictionController(CostPredictionService predictionService) {
        this.predictionService = predictionService;
    }


    @PostMapping("prediction")
    public double predictCost(@RequestBody PredictionParams params) {
        return  predictionService.defineCost(params);
    }
}
