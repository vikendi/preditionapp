package com.cryptoBot.prediction.dto;

import lombok.Data;

/**
 * DTO class for authentication (login) request.
 *
 * @author Nikita Tikhonov
 * @version 1.0
 */

@Data
public class AuthenticationRequestDto {
    private String username;
    private String password;
}
