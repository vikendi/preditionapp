package com.cryptoBot.prediction.dto;

public class PredictionParams {
    private double EnCons;
    private double Supply;
    private double MinDif;
    private double ChangeDin;
    private double TransactionPerDay;

    public PredictionParams(double enCons, double supply, double minDif, double changeDin, double transactionPerDay) {
        EnCons = enCons;
        Supply = supply;
        MinDif = minDif;
        ChangeDin = changeDin;
        TransactionPerDay = transactionPerDay;
    }

    public double getEnCons() { return EnCons; }

    public void setEnCons(double enCons) { EnCons = enCons; }

    public double getSupply() { return Supply; }

    public void setSupply(double supply) { Supply = supply; }

    public double getMinDif() { return MinDif; }

    public void setMinDif(double minDif) { MinDif = minDif; }

    public double getChangeDin() { return ChangeDin; }

    public void setChangeDin(double changeDin) { ChangeDin = changeDin; }

    public double getTransactionPerDay() { return TransactionPerDay; }

    public void setTransactionPerDay(double transactionPerDay) { TransactionPerDay = transactionPerDay; }
}
